-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: bookstore
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book_data`
--

DROP TABLE IF EXISTS `book_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_data` (
  `Booki_ID` int(11) DEFAULT NULL,
  `Title` varchar(25) DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Author` varchar(30) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `ISBN` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_data`
--

LOCK TABLES `book_data` WRITE;
/*!40000 ALTER TABLE `book_data` DISABLE KEYS */;
INSERT INTO `book_data` VALUES (1,'The complete reference',500,'Herbert Schildt',15,'978007180855'),(2,'Basic java program',350,'Steven Perry',15,'862207180123'),(3,'oop',450,'Danny c.c',15,'862204520123'),(4,'java pattern',230,'David landup',18,'785012520123'),(5,'Stack Abuse',154,'Danny',24,'85640259124'),(6,'java array',200,'Herbert',19,'978007180855'),(7,'Loop Control',190,'Peter',30,'258963147025'),(8,'Inheritance',250,'Dani shapiro',12,'158007115955'),(9,'Decision Making',390,'Esther',25,'89656314005'),(10,'Overriding',201,'Manasha',22,'902007145815'),(11,'Abstraction',180,'Mellita',12,'108963147015');
/*!40000 ALTER TABLE `book_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_purchase`
--

DROP TABLE IF EXISTS `book_purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_purchase` (
  `User_ID` int(11) DEFAULT NULL,
  `Book_ID` int(11) DEFAULT NULL,
  `Total_amount` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_purchase`
--

LOCK TABLES `book_purchase` WRITE;
/*!40000 ALTER TABLE `book_purchase` DISABLE KEYS */;
/*!40000 ALTER TABLE `book_purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loge`
--

DROP TABLE IF EXISTS `loge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loge` (
  `ID` int(11) DEFAULT NULL,
  `Date_and_time` date DEFAULT NULL,
  `modified_by` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loge`
--

LOCK TABLES `loge` WRITE;
/*!40000 ALTER TABLE `loge` DISABLE KEYS */;
/*!40000 ALTER TABLE `loge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopcard`
--

DROP TABLE IF EXISTS `shopcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopcard` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) DEFAULT NULL,
  `BookName` varchar(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `tPrice` double DEFAULT NULL,
  `add_date_Time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopcard`
--

LOCK TABLES `shopcard` WRITE;
/*!40000 ALTER TABLE `shopcard` DISABLE KEYS */;
INSERT INTO `shopcard` VALUES (7,'thilagshana','java array',2,400,'2019-05-18 03:20:21'),(8,'mathunujan','java array',1,200,'2019-05-18 04:15:35');
/*!40000 ALTER TABLE `shopcard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shoppingcard`
--

DROP TABLE IF EXISTS `shoppingcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shoppingcard` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  `date_time` date DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shoppingcard`
--

LOCK TABLES `shoppingcard` WRITE;
/*!40000 ALTER TABLE `shoppingcard` DISABLE KEYS */;
/*!40000 ALTER TABLE `shoppingcard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `address` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'thilagshana','smiley','thilagshana1998@gmail.com','kondavil'),(2,'mathunujan','mathu123','smathunujan@gmail.com','kokuvil'),(3,'faus','faus','faus@gmail.com','jaffna');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-18  9:54:58
