import java.sql.*;

public class ShopCardData {

	public static boolean checkBook(String bookcallno) {
		boolean status = false;
		try {
			Connection con = DatabaseConn.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from book_data where title=?");
			ps.setString(1, bookcallno);
			ResultSet rs = ps.executeQuery();
			status = rs.next();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static int save(String bookcallno) {
		int status = 0;
		Date date = null;
		double TPrice = 0;
		try {
			Connection con = DatabaseConn.getConnection();

			status = updatebook(bookcallno);// updating quantity and issue
			PreparedStatement ps1 = con.prepareStatement("select price from book_data where title=?");
			ps1.setString(1, bookcallno);
			ResultSet rs = ps1.executeQuery();
			rs.last();
			TPrice = Double.parseDouble(rs.getString(1));
			rs.beforeFirst();

			if (status > 0) {
				PreparedStatement ps = con
						.prepareStatement("insert into shopCard(user_name,BookName,qty,tprice) values(?,?,?,?)");
				ps.setString(1, Login.userName);
				ps.setString(2, bookcallno);
				ps.setInt(3, BookView.qty);
				ps.setDouble(4, TPrice * BookView.qty);
				// ps.setDate(5, date);
				status = ps.executeUpdate();
			}

			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static int updatebook(String bookcallno) {
		int status = 0;
		int quantity = 0, issued = 0;
		try {
			Connection con = DatabaseConn.getConnection();

			PreparedStatement ps = con.prepareStatement("select quantity from book_data where title=?");
			ps.setString(1, bookcallno);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				quantity = rs.getInt("quantity");
				// issued = rs.getInt("issued");
			}

			if (quantity > 0) {
				PreparedStatement ps2 = con.prepareStatement("update book_data set quantity=? where title=?");
				ps2.setInt(1, quantity - 1);
				// ps2.setInt(2, issued + 1);
				ps2.setString(2, bookcallno);

				status = ps2.executeUpdate();
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}
}
