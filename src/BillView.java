import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class BillView extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BillView frame = new BillView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BillView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textField = new JTextField();
		textField.setBounds(107, 34, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(26, 90, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(225, 90, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setBounds(133, 134, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		JLabel lblBookName = new JLabel("Book name");
		lblBookName.setBounds(124, 22, 69, 14);
		contentPane.add(lblBookName);

		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(44, 75, 69, 14);
		contentPane.add(lblQuantity);

		JLabel lblUnitPrice = new JLabel("Unit Price");
		lblUnitPrice.setBounds(238, 75, 69, 14);
		contentPane.add(lblUnitPrice);

		JLabel lblTotalPrice = new JLabel("Total Price");
		lblTotalPrice.setBounds(150, 121, 69, 14);
		contentPane.add(lblTotalPrice);

		JButton btnNewButton = new JButton("print");
		btnNewButton.setBounds(26, 199, 89, 23);
		contentPane.add(btnNewButton);

		JButton btnGoToMain = new JButton("Go to Main Menu");
		btnGoToMain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UserSearchForm.main(new String[] {});
			}
		});
		btnGoToMain.setBounds(275, 199, 124, 23);
		contentPane.add(btnGoToMain);
		try {
			textField.setText(UserSearchForm.bookcallno);
			textField_1.setText(String.valueOf(BookView.qty));
			Connection con = DatabaseConn.getConnection();

			PreparedStatement ps = con.prepareStatement("select price from book_data where title=?");
			ps.setString(1, UserSearchForm.bookcallno);

			ResultSet rs = ps.executeQuery();
			rs.last();
			textField_2.setText(rs.getString(1));
			rs.beforeFirst();
			double GetTotal = Double.parseDouble(textField_2.getText()) * BookView.qty;
			textField_3.setText(String.valueOf(GetTotal));
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
