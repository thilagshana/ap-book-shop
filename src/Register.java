import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class Register extends JFrame {
	static Register frame;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new Register();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Register() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(240, 128, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("User name :");
		lblNewLabel.setBounds(27, 22, 86, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Password :");
		lblNewLabel_1.setBounds(27, 91, 86, 14);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Email :");
		lblNewLabel_2.setBounds(27, 55, 46, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Address :");
		lblNewLabel_3.setBounds(27, 127, 54, 14);
		contentPane.add(lblNewLabel_3);

		textField = new JTextField();
		textField.setBackground(new Color(255, 255, 255));
		textField.setForeground(new Color(204, 51, 102));
		textField.setBounds(99, 19, 274, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setForeground(new Color(199, 21, 133));
		textField_1.setBounds(99, 47, 274, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setForeground(new Color(204, 51, 153));
		textField_2.setBounds(99, 124, 270, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String name = textField.getText();
				String password = String.valueOf(passwordField.getPassword());
				String email = textField_1.getText();
				String address = textField_2.getText();

				int i = UserData.save(name, password, email, address);
				if (i > 0) {
					JOptionPane.showMessageDialog(Register.this, "user added successfully!");
					Login.main(new String[] {});
					frame.dispose();

				} else {
					JOptionPane.showMessageDialog(Register.this, "Sorry, unable to save!");
				}
			}

		});
		btnOk.setBounds(183, 175, 89, 23);
		contentPane.add(btnOk);

		passwordField = new JPasswordField();
		passwordField.setForeground(new Color(199, 21, 133));
		passwordField.setBounds(99, 88, 274, 20);
		contentPane.add(passwordField);
	}
}
