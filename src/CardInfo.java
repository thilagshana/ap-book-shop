import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CardInfo extends JFrame {

	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JLabel label;
	private JLabel label_1;
	private JTextField textField;
	private JLabel label_2;
	private JTextField textField_5;
	private JButton btnConfiemOrder;
	private JButton btnBack;
	private JLabel lblCardDetail;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CardInfo frame = new CardInfo();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CardInfo() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 245, 238));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(39, 71, 46, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBounds(39, 108, 46, 14);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setBounds(28, 71, 46, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setBounds(237, 71, 46, 14);
		contentPane.add(lblNewLabel_3);

		textField_1 = new JTextField();
		textField_1.setForeground(new Color(0, 0, 139));
		textField_1.setBounds(103, 105, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setForeground(new Color(0, 0, 139));
		textField_2.setBounds(293, 68, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setForeground(new Color(0, 0, 139));
		textField_3.setBounds(293, 105, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setForeground(new Color(0, 0, 139));
		textField_4.setColumns(10);
		textField_4.setBounds(103, 65, 86, 20);
		contentPane.add(textField_4);

		label = new JLabel("New label");
		label.setBounds(237, 108, 46, 14);
		contentPane.add(label);

		label_1 = new JLabel("New label");
		label_1.setBounds(39, 152, 46, 14);
		contentPane.add(label_1);

		textField = new JTextField();
		textField.setForeground(new Color(0, 0, 139));
		textField.setColumns(10);
		textField.setBounds(103, 149, 86, 20);
		contentPane.add(textField);

		label_2 = new JLabel("New label");
		label_2.setBounds(237, 152, 46, 14);
		contentPane.add(label_2);

		textField_5 = new JTextField();
		textField_5.setForeground(new Color(0, 0, 139));
		textField_5.setColumns(10);
		textField_5.setBounds(293, 149, 86, 20);
		contentPane.add(textField_5);

		btnConfiemOrder = new JButton("Confim order");
		btnConfiemOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BillView.main(new String[] {});
			}
		});
		btnConfiemOrder.setBounds(179, 206, 126, 23);
		contentPane.add(btnConfiemOrder);

		btnBack = new JButton("Back");
		btnBack.setBounds(10, 206, 86, 23);
		contentPane.add(btnBack);

		lblCardDetail = new JLabel("Enter the Card Detail");
		lblCardDetail.setBounds(161, 11, 126, 14);
		contentPane.add(lblCardDetail);
	}
}
