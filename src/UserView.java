import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JToolBar;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UserView extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JLabel lblPrize;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UserView frame = new UserView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UserView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 204, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBookName = new JLabel("Book name :");
		lblBookName.setBounds(40, 96, 104, 19);
		contentPane.add(lblBookName);
		
		JLabel lblBookId = new JLabel("Book ID :");
		lblBookId.setBounds(40, 137, 104, 19);
		contentPane.add(lblBookId);
		
		JLabel lblNewLabel = new JLabel("User name :");
		lblNewLabel.setBounds(40, 11, 78, 19);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("User address :");
		lblNewLabel_1.setBounds(40, 59, 126, 14);
		contentPane.add(lblNewLabel_1);
		
		textField = new JTextField();
		textField.setForeground(new Color(153, 0, 51));
		textField.setBounds(50, 33, 306, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setForeground(new Color(153, 0, 51));
		textField_1.setBounds(50, 74, 306, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setForeground(new Color(153, 0, 51));
		textField_2.setBounds(50, 117, 306, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setForeground(new Color(153, 0, 51));
		textField_3.setBounds(50, 157, 155, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnOk = new JButton("Ok");
		btnOk.setBounds(315, 227, 89, 23);
		contentPane.add(btnOk);
		
		lblPrize = new JLabel("Prize :");
		lblPrize.setBounds(40, 179, 46, 14);
		contentPane.add(lblPrize);
		
		textField_4 = new JTextField();
		textField_4.setBounds(50, 196, 135, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
	}
}
