import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JTextField;

public class ShopCardList extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btnBuy;
	private JTextField textField;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShopCardList frame = new ShopCardList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ShopCardList() {
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 318);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		String data[][] = null;
		String column[] = null;
		try {
			Connection con = DatabaseConn.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from shopCard where user_name=?",
					ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ps.setString(1, Login.userName);
			ResultSet rs = ps.executeQuery();

			ResultSetMetaData rsmd = rs.getMetaData();
			int cols = rsmd.getColumnCount();
			column = new String[cols];
			for (int i = 1; i <= cols; i++) {
				column[i - 1] = rsmd.getColumnName(i);
			}

			rs.last();
			int rows = rs.getRow();
			rs.beforeFirst();

			data = new String[rows][cols];
			int count = 0;
			while (rs.next()) {
				for (int i = 1; i <= cols; i++) {
					data[count][i - 1] = rs.getString(i);
				}
				count++;
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		contentPane.setLayout(null);

		table = new JTable(data, column);
		JScrollPane sp = new JScrollPane(table);
		sp.setBounds(5, 5, 424, 175);

		contentPane.add(sp);
		
		btnBuy = new JButton("Buy");
		btnBuy.setBounds(335, 191, 89, 23);
		contentPane.add(btnBuy);
		
		textField = new JTextField();
		textField.setBounds(240, 192, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
	}

}
