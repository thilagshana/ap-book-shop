import java.sql.*;

public class BookViewData {

	public static boolean checkBook(String bookcallno) {
		boolean status = false;
		try {
			Connection con = DatabaseConn.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from book_data where title=?");
			ps.setString(1, bookcallno);
			ResultSet rs = ps.executeQuery();
			status = rs.next();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static int save(String bookcallno) {
		int status = 0;
		try {
			Connection con = DatabaseConn.getConnection();

			PreparedStatement ps = con.prepareStatement("select * from book_data where title=?");
			ps.setString(1, bookcallno);
			status = ps.executeUpdate();
			ResultSet rs = ps.executeQuery();
			rs.last();
			BookView.textField.setText(rs.getString(1));
			BookView.textField_1.setText(rs.getString(2));
			BookView.textField_4.setText(rs.getString(3));
			BookView.textField_3.setText(rs.getString(4));
			BookView.textField_2.setText(rs.getString(6));
			rs.beforeFirst();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}

	public static int updatebook(String bookcallno) {
		int status = 0;
		int quantity = 0, issued = 0;
		try {
			Connection con = DatabaseConn.getConnection();

			PreparedStatement ps = con.prepareStatement("select quantity,issued from books where callno=?");
			ps.setString(1, bookcallno);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				quantity = rs.getInt("quantity");
				issued = rs.getInt("issued");
			}

			if (quantity > 0) {
				PreparedStatement ps2 = con.prepareStatement("update books set quantity=?,issued=? where callno=?");
				ps2.setInt(1, quantity - 1);
				ps2.setInt(2, issued + 1);
				ps2.setString(3, bookcallno);

				status = ps2.executeUpdate();
			}
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return status;
	}
}
