import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;

public class BookView extends JFrame {
	static BookView frame;
	private JPanel contentPane;
	public static JTextField textField;
	public static JTextField textField_1;
	public static JTextField textField_2;
	public static JTextField textField_3;
	private JLabel lblPrize;
	public static JTextField textField_4;
	private JButton btnAddToShopping;
	private JTextField textField_5;
	private JLabel lblEnterQuantity;
	private JButton btnBuy;
	static int qty = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new BookView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BookView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 204, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblBookName = new JLabel("ISBN :");
		lblBookName.setBounds(50, 75, 40, 19);
		contentPane.add(lblBookName);

		JLabel lblBookId = new JLabel("Book ID :");
		lblBookId.setBounds(32, 22, 58, 19);
		contentPane.add(lblBookId);

		JLabel lblNewLabel = new JLabel("Book Name :");
		lblNewLabel.setBounds(32, 52, 78, 19);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Author :");
		lblNewLabel_1.setBounds(21, 108, 40, 14);
		contentPane.add(lblNewLabel_1);

		textField = new JTextField();
		textField.setForeground(new Color(153, 0, 51));
		textField.setBounds(92, 21, 173, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setForeground(new Color(153, 0, 51));
		textField_1.setBounds(102, 52, 181, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setForeground(new Color(153, 0, 51));
		textField_2.setBounds(92, 75, 118, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		textField_3 = new JTextField();
		textField_3.setForeground(new Color(153, 0, 51));
		textField_3.setBounds(71, 105, 155, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);

		JButton btnOk = new JButton("Back");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnOk.setBounds(10, 204, 69, 23);
		contentPane.add(btnOk);

		lblPrize = new JLabel("Prize :");
		lblPrize.setBounds(31, 139, 46, 14);
		contentPane.add(lblPrize);

		textField_4 = new JTextField();
		textField_4.setForeground(new Color(165, 42, 42));
		textField_4.setBounds(81, 136, 135, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);

		btnAddToShopping = new JButton("Add to shopping Card");
		btnAddToShopping.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				qty = Integer.parseInt(textField_5.getText());

				if (ShopCardData.checkBook(UserSearchForm.bookcallno)) {

					int i = ShopCardData.save(UserSearchForm.bookcallno);
					if (i > 0) {
						JOptionPane.showMessageDialog(BookView.this, "Book issued successfully!");
						ShopCardList.main(new String[] {});
						frame.dispose();

					} else {
						JOptionPane.showMessageDialog(BookView.this, "Sorry, unable to issue!");
					} // end of save if-else

				} else {
					JOptionPane.showMessageDialog(BookView.this, "Sorry, Callno doesn't exist!");
				} // end of checkbook if-else

			}
		});
		btnAddToShopping.setBounds(136, 204, 139, 23);
		contentPane.add(btnAddToShopping);

		textField_5 = new JTextField();
		textField_5.setForeground(new Color(165, 42, 42));
		textField_5.setColumns(10);
		textField_5.setBounds(263, 173, 78, 20);
		contentPane.add(textField_5);

		lblEnterQuantity = new JLabel("Enter quantity");
		lblEnterQuantity.setBounds(190, 176, 69, 14);
		contentPane.add(lblEnterQuantity);

		btnBuy = new JButton("Buy");
		btnBuy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				qty = Integer.parseInt(textField_5.getText());
				if (ShopCardData.checkBook(UserSearchForm.bookcallno)) {

					int i = ShopCardData.save(UserSearchForm.bookcallno);
					if (i > 0) {

						CardInfo.main(new String[] {});
						frame.dispose();

					} else {
						JOptionPane.showMessageDialog(BookView.this, "Sorry, unable to issue!");
					} // end of save if-else

				} else {
					JOptionPane.showMessageDialog(BookView.this, "Sorry, Callno doesn't exist!");
				} // end of checkbook if-else
			}
		});
		btnBuy.setBounds(335, 204, 89, 23);
		contentPane.add(btnBuy);
		try {
			Connection con = DatabaseConn.getConnection();

			PreparedStatement ps = con.prepareStatement("select * from book_data where title=?");
			ps.setString(1, UserSearchForm.bookcallno);

			ResultSet rs = ps.executeQuery();
			rs.last();
			BookView.textField.setText(rs.getString(1));
			BookView.textField_1.setText(rs.getString(2));
			BookView.textField_4.setText(rs.getString(3));
			BookView.textField_3.setText(rs.getString(4));
			BookView.textField_2.setText(rs.getString(6));
			rs.beforeFirst();
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
